package com.example.emailsender;

import com.example.emailsender.email.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class EmailSenderApplicationTests {
    @Autowired
    private EmailService emailservice;

    @Test
    void contextLoads() {
        emailservice.send("ahmedbishree", "ahmedbishree@gmail.com");
    }

}
