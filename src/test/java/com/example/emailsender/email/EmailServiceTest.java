package com.example.emailsender.email;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
class EmailServiceTest {

    @Autowired
    private EmailService emailService;
    @Test
    void send() {
        emailService.send("ahmedbishree@gmail.com", "ahmedbishree@gmail.com");
    log.info("email sent successfully");
    }
}