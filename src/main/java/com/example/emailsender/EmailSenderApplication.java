package com.example.emailsender;

import com.example.emailsender.email.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EmailSenderApplication {

    public static void main(String[] args) {

        SpringApplication.run(EmailSenderApplication.class, args);
    }



}
